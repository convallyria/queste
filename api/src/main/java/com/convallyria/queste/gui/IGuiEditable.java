package com.convallyria.queste.gui;

public interface IGuiEditable {

    /**
     * User friendly name.
     * @return name of this implementation
     */
    String getName();
}
